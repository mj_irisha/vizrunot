import React, { Component, Fragment } from "react";
import { Route, Switch } from 'react-router-dom';
import About from "./Pages/About/About";
import Faq from "./Pages/FAQ/Faq";
import Contact from "./Pages/Contact/Contact";
import Advertisement from "./Pages/Advertisement/Advertisement";

class SimplePagesLayout extends Component {


  render() {
    return (
      <Fragment>
        <div>Header Simple</div>
        <Switch>
          <Route path="/about" component={About} />
          <Route path="/faq" component={Faq} />
          <Route path="/contact" component={Contact} />
          <Route path="/ads" component={Advertisement} />
        </Switch>
      </Fragment>
    );
  }
}
export default SimplePagesLayout;
