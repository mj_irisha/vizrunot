import React, { Component } from "react";
import { Link, NavLink } from "react-router-dom";
import "../../assets/css/components/header.css";
import logo from "../../assets/images/logo.svg";
import MJModal from "../Elements/MJModal";

class Header extends Component {
  openLoginModal = () => {
    this.loginModal.handleOpenModal();
  };

  render() {
    return (
      <header>
        <Link to="/" className="logo">
          <img src={logo} alt="ვიზრუნოთ ლოგო" />
        </Link>
        <nav className="nav nav__main">
          <NavLink
            exact
            to="/"
            activeClassName="active"
            className="link link__menu-item"
          >
            მთავარი გვერდი
          </NavLink>
          <NavLink
            to="/about"
            activeClassName="active"
            className="link link__menu-item"
          >
            ჩვენ შესახებ
          </NavLink>
          <NavLink
            to="/faq"
            activeClassName="active"
            className="link link__menu-item"
          >
            კითხვა-პასუხი
          </NavLink>
          <NavLink
            to="/contact"
            activeClassName="active"
            className="link link__menu-item"
          >
            კონტაქტი
          </NavLink>
          <NavLink
            to="/ads"
            activeClassName="active"
            className="link link__menu-item"
          >
            რეკლამა საიტზე
          </NavLink>
        </nav>

        <div className="btns">
          <div className="btn btn-high btn-light lang">
            <span>geo</span>
            {/* <a href="" className="lang_item">eng</a>
            <a href="" className="lang_item">rus</a> */}
          </div>

          <div
            className="btn btn-high btn-blue profile"
            onClick={this.openLoginModal}
          >
            <span className="profile__item">ჩემი ანგარიში</span>
          </div>
        </div>

        <MJModal ref={modal => (this.loginModal = modal)} title="ჩემი ანგარიში">
          <form>
            <div className="input-out">
              <input type="text" className="input" placeholder="შეიყვანეთ მობილურის ნომერი"/>
            </div>
            <div className="input-out">
              <input type="password" className="input" placeholder="პაროლი"/>
            </div>
            <div className="input-out">
              <input type="submit" className="btn btn-form btn-blue btn-high" value="ავტორიზაცია"/>
            </div>
          </form>
          <div className="pointer">დაგავიწყდათ პაროლი?</div>
          <button className="btn btn-form btn-yellow btn-high">რეგისტრაცია</button>
        </MJModal>
      </header>
    );
  }
}

export default Header;
