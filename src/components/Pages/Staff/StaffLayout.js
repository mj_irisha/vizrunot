import React, { Component, Fragment } from "react";
import StickyBar from "../../Blocks/StickyBar";
import Staff from "./Staff";

class StaffLayout extends Component {
  // state = {
  //   activeComponent: "nannies"
  // };

  render() {
    return (
      <Fragment>
        <StickyBar />
        <Staff />
      </Fragment>
    );
  }
}

export default StaffLayout;
