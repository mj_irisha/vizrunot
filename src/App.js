import React, { Component } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import "./assets/css/main.css";
import Header from "./components/Blocks/Header";
import HomeLayout from "./components/Pages/Home/HomeLayout";
import StaffLayout from "./components/Pages/Staff/StaffLayout";
import SimplePagesLayout from "./components/SimplePagesLayout";
import StaffDetails from "./components/Pages/Staff/StaffDetails";

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="App">
          <Header />
          <Switch>
            <Route exact path="/" component={HomeLayout} />
            <Route exact path={["/staff", "/staff/:staff/:name"]} component={StaffDetails} />
            <Route path={["/staff", "/staff/:staff"]} component={StaffLayout} />
            
  
            <Route
              path={["/about", "/contact", "/faq", "/ads"]}
              component={SimplePagesLayout}
            />
          </Switch>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
