import React, { Component, Fragment } from "react";

class Slider extends Component {
  state = {
    activeSlide: 0,
    auto: 3000,
    firstSlide: false,
    lastSlide: false
  };

  componentDidMount = () => {
    if (this.props.auto) {
      if (this.props.auto > 1) {
        this.setState({ auto: this.props.auto }, () =>
          this.slidesAutoChangeHandler(this.state.auto)
        );
      } else {
        this.slidesAutoChangeHandler(this.state.auto);
      }
    }

    if (this.state.activeSlide === 0) {
      this.setState({ firstSlide: true });
    }
  };

  // interesting, but prevProps is needed to prevent repeating envokes!
  componentDidUpdate = (prevProps, prevState) => {
    if (this.props.auto && prevState.activeSlide !== this.state.activeSlide) {
      this.slidesAutoChangeHandler(this.state.auto);
    }
  };

  sliderDotClickHandler = ev => {
    const newSlideIndex = this.props.slidesData.findIndex(
      x => x.id === +ev.target.dataset.sliderId
    );

    this.setState({ activeSlide: newSlideIndex }, () =>
      this.props.changeSlideHandler(newSlideIndex)
    );
  };

  nextSliderHandler = () => {
    this.setState({ firstSlide: false });

    let newSlide = this.state.activeSlide + 1;
    let length = Math.ceil(this.props.slidesData.length / 4);

    if (newSlide === length - 1) {
      this.setState({ lastSlide: true });
    }

    this.setState({ activeSlide: newSlide }, () =>
      this.props.changeSlideHandler(newSlide)
    );
  };

  prevSliderHandler = () => {
    this.setState({ lastSlide: false });

    let newSlide = this.state.activeSlide - 1;

    if (newSlide === 0) {
      this.setState({ firstSlide: true });
    }

    this.setState({ activeSlide: newSlide }, () =>
      this.props.changeSlideHandler(newSlide)
    );
  };

  slidesAutoChangeHandler = ms => {
    let newSlide = this.state.activeSlide + 1;
    if (this.state.activeSlide === this.props.slidesData.length - 1) {
      newSlide = 0;
    }

    setTimeout(() => {
      this.setState({ activeSlide: newSlide }, () => {
        this.props.changeSlideHandler(this.state.activeSlide);
      });
    }, ms);
  };

  render() {
    const { className, children } = this.props;
    const mainClass = className ? " " + className : "";

    return (
      <div className={`slider${mainClass}`}>
        {children}
        {this.props.dots &&
          this.props.slidesData &&
          this.props.slidesData.length > 1 && (
            <div className="slider_dots">
              {this.props.slidesData.map((slide, index) => (
                <span
                  key={slide.id}
                  className={`slider_dot${
                    index === this.state.activeSlide ? " slider_dot-active" : ""
                  }`}
                  data-slider-id={slide.id}
                  onClick={this.sliderDotClickHandler}
                />
              ))}
            </div>
          )}
        {this.props.arrows && (
          <Fragment>
            {!this.state.firstSlide && (
              <div onClick={this.prevSliderHandler}>prev</div>
            )}
            {!this.state.lastSlide && (
              <div onClick={this.nextSliderHandler}>next</div>
            )}
          </Fragment>
        )}
      </div>
    );
  }
}

export default Slider;
