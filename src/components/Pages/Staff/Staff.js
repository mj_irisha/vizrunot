import React, { Component, Fragment } from "react";
import SinglePersonBlock from "../../Elements/SinglePersonBlock";
import SecondarySliderData from "../../../data/SecondarySliderData";

class Staff extends Component {
  render() {
    return (
      <section className="section">
        <div className="persons_grid">
          {SecondarySliderData.map(person => (
            <SinglePersonBlock person={person} />
          ))}
        </div>
      </section>
    );
  }
}

export default Staff;
