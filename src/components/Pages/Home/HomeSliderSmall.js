import React, { Component, Fragment } from "react";
import SecondarySliderData from "../../../data/SecondarySliderData";
import Slider from "../../HOC/Slider";

const Slide = ({ slideData }) => {
  return (
    <div className="slide person_block">
      <div className="person_block-title">
        <div className="person_block-style">
          მომვლელები <span>რეიტინგის მიხედვით</span>
        </div>
      </div>
      <div className="person_block-image">
        <img src={slideData.image} alt={slideData.name}/>
      </div>
      <div className="person_info">
        <div className="person_info_style">
          <span className="cat_style">სახელი </span>: {slideData.name}
        </div>
        <div className="person_info_style">
          <span className="cat_style">ასაკი </span>: {slideData.age} წლის
        </div>
        <div className="person_info_style">
          <span className="cat_style">ქალაქი </span>: {slideData.city}
        </div>
      </div>
    </div>
  );
};

class HomeSliderSmall extends Component {
  state = {
    activeSlideIndex: 0
  };

  changeSlideHandler = index => {
    this.setState({ activeSlideIndex: index });
  };

  render() {
    return SecondarySliderData ? (
      <section className="section section-slider">
        <Slider
          className="slider_secondary"
          slidesData={SecondarySliderData}
          changeSlideHandler={this.changeSlideHandler}
          auto={6000}
        >
          <Slide slideData={SecondarySliderData[this.state.activeSlideIndex]} />
        </Slider>
      </section>
    ) : (
      <Fragment />
    );
  }
}

export default HomeSliderSmall;
