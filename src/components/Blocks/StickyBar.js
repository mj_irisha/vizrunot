import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import MJSelect from "../Elements/Select";

const personOptions = [
  { value: 'ძიძას - 24-საათიანი გრაფიკით', label: 'ძიძას - 24-საათიანი გრაფიკით' },
  { value: 'ძიძას - საათობრივად', label: 'ძიძას - საათობრივად' },
  { value: 'ძიძა-სტუდენტს', label: 'ძიძა-სტუდენტს' },
  { value: 'ძიძას - სტანდარტული გრაფიკით', label: 'ძიძას - სტანდარტული გრაფიკით' },
  { value: 'ძიძა + დამხმარე', label: 'ძიძა + დამხმარე' },
  { value: 'ძიძას-აღმზრდელს<', label: 'ძიძას-აღმზრდელს<' }
]

const cityOptions = [
  { value: 'tbilisi', label: 'tbilisi' },
  { value: 'prague', label: 'prague' }
]

class StickyBar extends Component {

  render() {
    return <div className="sticky">
    <form className="search-box" id="searchForm">
      <div className="label-type">მე ვეძებ</div>

      <div className="select">
        <MJSelect options={personOptions} />
      </div>
      <div className="select">
        <MJSelect options={cityOptions} />
      </div>
      <button className="search_button"></button>
    </form>
    <div className="btns">
      <NavLink className="btn btn-transparent" to="/staff/nannies">ძიძები</NavLink>
      <NavLink className="btn btn-transparent" to="/staff/sitters">მომვლელები</NavLink>
      <NavLink className="btn btn-transparent" to="/staff/housemaids">დამლაგებლები</NavLink>
    </div>
  </div>;
  }
}
export default StickyBar;
