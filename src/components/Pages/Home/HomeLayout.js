import React, { Component, Fragment } from 'react';
import HomeSlider from './HomeSlider';
import HomeSliderSmall from './HomeSliderSmall';
import '../../../assets/css/pages/home.css';
import StickyBar from '../../Blocks/StickyBar';
import HomeVipSlider from './HomeVipSlider';
import HomeNewPersons from './HomeNewPersons';


class HomeLayout extends Component{
  render(){
    return <Fragment>
        <div className="sliders_wrapper">
          <HomeSlider />
          <HomeSliderSmall />  
        </div>
        <StickyBar />
        <HomeVipSlider />
        <HomeNewPersons />
      </Fragment> 
  }

}

export default HomeLayout;