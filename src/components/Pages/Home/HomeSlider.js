import React, { Component, Fragment } from "react";
import mainSliderData from "../../../data/MainSliderData";
import Slider from "../../HOC/Slider";

const Slide = ({ slideData }) => {
  return (
    <div
      className="slide"
      style={{ backgroundImage: `url(${slideData.image})` }}
    >
      <div className="slide_info">
        <div className="slide_title">{slideData.title}</div>
        <div className="slide_text">{slideData.text}</div>
      </div>
    </div>
  );
};

class HomeSlider extends Component {
  state = {
    activeSlideIndex: 0
  };

  changeSlideHandler = index => {
    this.setState({ activeSlideIndex: index });
  };

  render() {
    return mainSliderData ? (
      <Slider
        className="slider_main"
        slidesData={mainSliderData}
        changeSlideHandler={this.changeSlideHandler}
        dots
      >
        <Slide slideData={mainSliderData[this.state.activeSlideIndex]} />
      </Slider>
    ) : (
      <Fragment />
    );
  }
}

export default HomeSlider;
