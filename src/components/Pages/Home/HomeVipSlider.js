import React, { Component } from "react";
import SecondarySliderData from "../../../data/SecondarySliderData";
import Slider from "../../HOC/Slider";
import SinglePersonBlock from "../../Elements/SinglePersonBlock";

class HomeVipSlider extends Component {
  state = {
    activeSlideIndex: 0,
    groupedSlides: null
  };

  componentDidMount() {
    let row = [];

    // group persons by 4
    let group = SecondarySliderData.reduce((ar, item, index) => {
      row.push(item);

      if ((index + 1) % 4 === 0) {
        ar.push(row);
        row = [];
      } else if (SecondarySliderData.length - 1 === index) {
        ar.push(row);
      }

      return ar;
    }, []);

    this.setState({ groupedSlides: group });
  }

  changeSlideHandler = index => {
    this.setState({ activeSlideIndex: index });
  };

  render() {
    return (
      <div className="bg-white">
        <section className="section">
          <div className="section-header">
            <div className="section-title blue">VIP განცხადებები</div>
          </div>
          {this.state.groupedSlides && (
            <Slider
              className="slider_vip persons_grid"
              slidesData={SecondarySliderData}
              changeSlideHandler={this.changeSlideHandler}
              slides={4}
              arrows
            >
              {this.state.groupedSlides[this.state.activeSlideIndex].map(
                person => (
                  <SinglePersonBlock person={person} />
                )
              )}
            </Slider>
          )}
        </section>
      </div>
    );
  }
}

export default HomeVipSlider;
