import React from "react";
import { Link } from "react-router-dom";

const SinglePersonBlock = ({ person }) => {
  let linkName = person.name.split(' ').join('-');
  
  return <Link key={person.id} to={`/staff/nannies/${linkName}`} className="person_block">
    <div className="person_block-image">
      <img src={person.image} alt={person.name} />
      <div className="person_block-prof">{person.profession}</div>
    </div>
    <div className="person_info">
      <div className="person_info_style">
        <span className="cat_style">სახელი </span>: {person.name}
      </div>
      <div className="person_info_style">
        <span className="cat_style">ასაკი </span>: {person.age} წლის
      </div>
      <div className="person_info_style">
        <span className="cat_style">ქალაქი </span>: {person.city}
      </div>
    </div>
  </Link>;
};

export default SinglePersonBlock;
