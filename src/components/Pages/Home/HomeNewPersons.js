import React from "react";
import SecondarySliderData from "../../../data/SecondarySliderData";
import SinglePersonBlock from "../../Elements/SinglePersonBlock";

const HomeNewPersons = props => {
  return (
    <section className="section">
      <div className="section-header">
        <div className="section-title">ახალი დამატებული</div>
      </div>
      <div className="persons_grid">
        {SecondarySliderData.map(person => (
          <SinglePersonBlock person={person} />
        ))}
      </div>
    </section>
  );
};

export default HomeNewPersons;
