import React, { Component, Fragment } from "react";
import ReactModal from "react-modal";

class MJModal extends Component {
  state = {
    showModal: false
  };

  handleOpenModal = () => {
    this.setState({ showModal: true });
  };

  handleCloseModal = () => {
    this.setState({ showModal: false });
  };

  render() {
    return (
      <ReactModal
        isOpen={this.state.showModal}
        overlayClassName="modal-overlay"
        className="modal"
        onRequestClose={() => {
          this.setState({ showModal: false });
        }}
      >
        <div className="modal-header">
          <div>{this.props.title}</div>
          <div onClick={this.handleCloseModal} className="modal-close">
            close
          </div>
        </div>
        <div className="modal-body">{this.props.children}</div>
      </ReactModal>
    );
  }
}

export default MJModal;
