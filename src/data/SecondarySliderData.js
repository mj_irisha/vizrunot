import baseUrl from '../constants/baseUrl';

const SecondarySliderData = [
  {
    id: 1,
    name: 'ინგა გროსმანი',
    age: '23',
    image: baseUrl + '/People/1/1.png',
    city: 'tbilisi',
    professionId: 'baby-sitter',
    profession: 'ძიძა'
  },
  {
    id: 2,
    name: 'ინგა გროსმანი',
    age: '23',
    image: baseUrl + '/People/2/2.png',
    city: 'tbilisi',
    professionId: 'baby-sitter',
    profession: 'ძიძა'
  }
  ,
  {
    id: 3,
    name: 'ინგა გროსმანი',
    age: '23',
    image: baseUrl + '/People/3/3.png',
    city: 'tbilisi',
    professionId: 'baby-sitter',
    profession: 'ძიძა'
  },
  {
    id: 4,
    name: 'ინგა გროსმანი',
    age: '23',
    image: baseUrl + '/People/1/1.png',
    city: 'tbilisi',
    professionId: 'baby-sitter',
    profession: 'ძიძა'
  },
  {
    id: 5,
    name: 'ინგა გროსმანი',
    age: '23',
    image: baseUrl + '/People/2/2.png',
    city: 'tbilisi',
    professionId: 'baby-sitter',
    profession: 'ძიძა'
  }
  ,
  {
    id: 6,
    name: 'ინგა გროსმანი',
    age: '23',
    image: baseUrl + '/People/3/3.png',
    city: 'tbilisi',
    professionId: 'baby-sitter',
    profession: 'ძიძა'
  }
];

export default SecondarySliderData;